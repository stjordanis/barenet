﻿using System;

namespace BareNET.Schema
{
    public enum Token
    {
        Type, // type
        Enum, // enum
        UInt, // uint
        U8, // u8
        U16, // u16
        U32, // u32
        U64, // u64
        Int, // int
        I8, // i8
        I16, // i16
        I32, // i32
        I64, // i64
        F32, // f32
        F64, // f64
        Bool, // bool
        String, // string
        Void, // void
        Data, // data
        Map, // map
        Optional, // optional
        LeftAngle, // <
        RightAngle, // >
        LeftParen, // (
        RightParen, // )
        LeftBracket, // [
        RightBracket, // ]
        LeftBrace, // {
        RightBrace, // }
        Pipe, // |
        Equal, // =
        Colon, // :
        Word, // [AZ-az][0-9]
        Digit // [0-9]
    }

    public static class Lexer
    {
        public static (Token token, string value)? Next(Scanner scanner)
        {
            var c = scanner.ReadChar();
            if (!c.HasValue) return null;

            var character = c.Value;
            if (char.IsDigit(character)) return (Token.Digit, scanner.ReadDigit(character));
            if (char.IsLetter(character))
            {
                var word = scanner.ReadWord(character);
                return word switch
                {
                    "type" => (Token.Type, string.Empty),
                    "enum" => (Token.Enum, string.Empty),
                    "uint" => (Token.UInt, string.Empty),
                    "u8" => (Token.U8, string.Empty),
                    "u16" => (Token.U16, string.Empty),
                    "u32" => (Token.U32, string.Empty),
                    "u64" => (Token.U64, string.Empty),
                    "int" => (Token.Int, string.Empty),
                    "i8" => (Token.I8, string.Empty),
                    "i16" => (Token.I16, string.Empty),
                    "i32" => (Token.I32, string.Empty),
                    "i64" => (Token.I64, string.Empty),
                    "f32" => (Token.F32, string.Empty),
                    "f64" => (Token.F64, string.Empty),
                    "bool" => (Token.Bool, string.Empty),
                    "string" => (Token.String, string.Empty),
                    "void" => (Token.Void, string.Empty),
                    "data" => (Token.Data, string.Empty),
                    "map" => (Token.Map, string.Empty),
                    "optional" => (Token.Optional, string.Empty),
                    _ => (Token.Word, word)
                };
            }

            return character switch
            {
                '<' => (Token.LeftAngle, string.Empty),
                '>' => (Token.RightAngle, string.Empty),
                '(' => (Token.LeftParen, string.Empty),
                ')' => (Token.RightParen, string.Empty),
                '[' => (Token.LeftBracket, string.Empty),
                ']' => (Token.RightBracket, string.Empty),
                '{' => (Token.LeftBrace, string.Empty),
                '}' => (Token.RightBrace, string.Empty),
                '|' => (Token.Pipe, string.Empty),
                '=' => (Token.Equal, string.Empty),
                ':' => (Token.Colon, string.Empty),
                _ => throw new FormatException($"Unknown token {c}")
            };
        }
    }
}
