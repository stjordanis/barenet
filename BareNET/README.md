# BareNET [![NuGet](https://badgen.net/nuget/v/BareNET)](https://www.nuget.org/packages/BareNET)

.NET-Implementation of Binary Application Record Encoding (BARE) - https://baremessages.org/

Implements the specification from the [draft](https://datatracker.ietf.org/doc/draft-devault-bare/) version 1.

## Usage
### Disclaimer
This document will not cover BARE basics. Please refer to the [official documentation](https://baremessages.org/).

### Install package
With dotnet-CLI
```cmd
dotnet add package BareNET
```

With [paket](https://fsprojects.github.io/Paket/)
```cmd
paket add BareNET
```

### Using a schema file
The prefered way is to use a schema file and let the code generator do the rest.

#### Install code generator
The code generator is available as dotnet tool and requires .NET 5!

```cmd
dotnet tool install bare-cli
```

#### Write the schema
Save the following content into `schema.bare`:
```
type PublicKey data<128>
type Time string # ISO 8601

enum Department {
    ACCOUNTING
    ADMINISTRATION
    CUSTOMER_SERVICE
    DEVELOPMENT

    # Reserved for the CEO
    JSMITH = 99
}

type Customer {
    name: string
    email: string
    address: Address
    orders: []{
        orderId: i64
        quantity: i32
    }
    metadata: map[string]data
}

type Employee {
    name: string
    email: string
    address: Address
    department: Department
    hireDate: Time
    publicKey: optional<PublicKey>
    metadata: map[string]data
}

type TerminatedEmployee void

type Person (Customer | Employee | TerminatedEmployee)

type Address {
    address: [4]string
    city: string
    state: string
    country: string
}
```

#### Generate the code
We take our schema file and let the generator save the code to `messages.cs`.

```cmd
dotnet bare schema.bare messages.cs
```

The code generation provides some options to change the output. To see all options use
```cmd
dotnet bare --help
```

### Writing your own encoder / decoder
BareNET delivers the building blocks to let you write your own encoder / decoder.

#### Encoders

| BARE type             | C# type           | method                                               |
|-----------------------|:-----------------:|:-----------------------------------------------------|
| uint                  | `ulong`           | `Bare.Encode_uint`                                   |
| u8                    | `byte`            | `Bare.Encode_u8`                                     |
| u16                   | `ushort`          | `Bare.Encode_u16`                                    |
| u32                   | `uint`            | `Bare.Encode_u32`                                    |
| u64                   | `ulong`           | `Bare.Encode_u64`                                    |
| int                   | `long`            | `Bare.Encode_int`                                    |
| i8                    | `sbyte`           | `Bare.Encode_i8`                                     |
| i16                   | `short`           | `Bare.Encode_i16`                                    |
| i32                   | `int`             | `Bare.Encode_i32`                                    |
| i64                   | `long`            | `Bare.Encode_i64`                                    |
| f32                   | `float`           | `Bare.Encode_f32`                                    |
| f64                   | `double`          | `Bare.Encode_f64`                                    |
| bool                  | `bool`            | `Bare.Encode_bool`                                   |
| string                | `string`          | `Bare.Encode_string`                                 |
| enum                  | `System.Enum`     | `Bare.Encode_enum`                                   |
| data                  | `byte[]`          | `Bare.Encode_data`                                   |
| data<length>          | `byte[]`          | `Bare.Encode_data_fixed_length`                      |
| void                  | None              | Not encoded                                          |
| optional<type>        | `T?` or `T`       | `Bare.Encode_optional` or `Bare.Encode_optional_ref` |
| [length]type          | `IEnumerable<T>`  | `Bare.Encode_list_fixed_length`                      |
| []type                | `IEnumerable<T>`  | `Bare.Encode_list`                                   |
| map[type A]type B     | `Dictionary<A,B>` | `Bare.Encode_map`                                    |
| (type \| type \| ...) | `T`               | `Bare.Encode_union`                                  |
| struct                | None              | Concatenate encoded field values                     |

#### Decoders

| BARE type             | C# type           | method                                               |
|-----------------------|:-----------------:|:-----------------------------------------------------|
| uint                  | `ulong`           | `Bare.Decode_uint`                                   |
| u8                    | `byte`            | `Bare.Decode_u8`                                     |
| u16                   | `ushort`          | `Bare.Decode_u16`                                    |
| u32                   | `uint`            | `Bare.Decode_u32`                                    |
| u64                   | `ulong`           | `Bare.Decode_u64`                                    |
| int                   | `long`            | `Bare.Decode_int`                                    |
| i8                    | `sbyte`           | `Bare.Decode_i8`                                     |
| i16                   | `short`           | `Bare.Decode_i16`                                    |
| i32                   | `int`             | `Bare.Decode_i32`                                    |
| i64                   | `long`            | `Bare.Decode_i64`                                    |
| f32                   | `float`           | `Bare.Decode_f32`                                    |
| f64                   | `double`          | `Bare.Decode_f64`                                    |
| bool                  | `bool`            | `Bare.Decode_bool`                                   |
| string                | `string`          | `Bare.Decode_string`                                 |
| enum                  | `System.Enum`     | `Bare.Decode_enum`                                   |
| data                  | `byte[]`          | `Bare.Decode_data`                                   |
| data<length>          | `byte[]`          | `Bare.Decode_data_fixed_length`                      |
| void                  | None              | Not decoded                                          |
| optional<type>        | `T?` or `T`       | `Bare.Decode_optional` or `Bare.Decode_optional_ref` |
| [length]type          | `IEnumerable<T>`  | `Bare.Decode_list_fixed_length`                      |
| []type                | `IEnumerable<T>`  | `Bare.Decode_list`                                   |
| map[type A]type B     | `Dictionary<A,B>` | `Bare.Decode_map`                                    |
| (type \| type \| ...) | `T`               | `Bare.Decode_union`                                  |
| struct                | None              | Pipe rest value from field values decoders           |

#### Unions
Encoders and Decoders for unions need a definition of the union to know what to encode/decode.

BareNET comes with an easy to use API to define a union:
```csharp
var union =
    Union<object>.Register()
        .With_Case<int>(v => BareNET.Bare.Encode_i32((int)v), d => BareNET.Bare.Decode_i32(d))
        .With_Case<string>(v => BareNET.Bare.Encode_string((string)v), d => BareNET.Bare.Decode_string(d))
        .With_Case_identified_by<float>(2, v => BareNET.Bare.Encode_f32((float)v), d => BareNET.Bare.Decode_f32(d))
        .With_Case<bool>(v => BareNET.Bare.Encode_bool((bool)v), d => BareNET.Bare.Decode_bool(d))
        .With_Case<ulong>(value => BareNET.Bare.Encode_uint((ulong)value), data => BareNET.Bare.Decode_uint(data))
        .With_Case<char>(v => BareNET.Bare.Encode_i32(Convert.ToInt32((char)v)), d => { var (v, r) = BareNET.Bare.Decode_i32(d); return ((char)v, r); })
        .With_Case<byte[]>(v => BareNET.Bare.Encode_data((byte[])v), d => BareNET.Bare.Decode_data(d));

var values = new object[]
    {
        2,
        2.32f,
        "Hello World",
        true,
        2UL,
        'A',
        new byte[] { 0x01, 0x02, 0x03, 0x04 }
    };

foreach (var value in values)
{
    var (decoded, _) = BareNET.Bare.Decode_union<object>(BareNET.Bare.Encode_union(value, union), union);
    // decoded == (object) value
}
```

**WAIT!** What is `With_Case_identified_by`?

Unions in BARE are great for versioning types. Each case is identified by a number starting with 0. For example we have messages in different versions:
```csharp
Union<object>.Register()
    .With_Case<MessageV1>(MessageV1_Encoder, MessageV1_Decoder)
    .With_Case<MessageV2>(MessageV2_Encoder, MessageV2_Decoder)
    .With_Case<MessageV3>(MessageV3_Encoder, MessageV3_Decoder);
```

If we decide that we don't want to support MessageV1 anymore and delete all the code, then the union will break backwards compatiblity. Here we make use of `With_Case_identified_by` by declaring the union starts with the intendifier by 1. All other case identifieres will increment from the given identifier.
```csharp
Union<object>.Register()
    .With_Case_identified_by<MessageV2>(1, MessageV2_Encoder, MessageV2_Decoder)
    .With_Case<MessageV3>(MessageV3_Encoder, MessageV3_Decoder);
```

#### Structs
Structs are just the values concatenated. Let's take an example with an address struct.

```csharp
struct Address
{
    public readonly string[] Address;
    public readonly string City;
    public readonly string State;
    public readonly string Country;

    public Address(string[] address, string city, string state, string country)
    {
        if (address.Length != 4) throw new System.Exception("Address can only contain 4 parts");
        Address = address;
        City = city;
        State = state;
        Country = country;
    }
}
```

To encode the struct we take each value, encode it and concatenate the results.

```csharp
using BareNET;
using System.Linq;
byte[] Address_Encoded(Address address)
{
    return Bare.Encode_list_fixed_length(4, address.Address, Bare.Encode_string)
        .Concat(Bare.Encode_string(address.City))
        .Concat(Bare.Encode_string(address.State))
        .Concat(Bare.Encode_string(address.Country))
        .ToArray();
}
```


Great! We have our address encoder. Let's go to the decoder! Every decoder in BareNET takes the data as `byte[]` and gives back a tuple `(T value, byte[] rest)`.
The first element is the decoded value and the second is the data that was not used to decode the value.

So if we take our bytes from the encoded struct `Address` and parse the first field `string[] Address`, we get the `string[]` value and the rest of the bytes.
```csharp
var (address, rest) = Bare.Decode_list_fixed_length(4, data, Bare.Decode_string);
// address = byte[4]
// rest = byte[] containing encoded City, State & Country
```


In order to contain all values we take the rest data and put it in the next decoder until we got our `Address`

```csharp
using BareNET;
(Address value, byte[] rest) Decode_Address(byte[] data)
{
    var (address, addressRest) = Bare.Decode_list_fixed_length(4, data, Bare.Decode_string);
    var (city, cityRest) = Bare.Decode_string(addressRest);
    var (state, stateRest) = Bare.Decode_string(cityRest);
    var (country, rest) = Bare.Decode_string(stateRest);
    return (
        new Address(address, city, state, country),
        rest
    );
}
```


Yeah! We have our encoder and decoder. But if we have data which only contains the encoded `Address`, then why don't we just get our `Address` value?

Well, this decoder allows us to use the `Address` struct in another type, like optional, map, list or in an union. To obtain only the `Address` value we simple use the just written decoder and access only the value.

```csharp
// with destructing
var (myAddress, _) = Decode_Address(data);
// or member access
var myAddress = Decode_Address(data).value;
// or using a function
Address Address_Decoded(byte[] data) => Decode_Address(data).value;
var myAddress = Address_Decoded(data);
```

## How do I encode types like Guid or DateTime?
Great question! BARE supports a few primitive and aggregate types and won't support more. The reason behind this is to be closed and simple but extensible, so that you can share your schema between other implementations.

To archive the encoding and decoding of other types which are language specific, you have to convert the specific type into a BARE type. Let's take a look at the `Guid` type.
We can convert a `Guid` to `string` or `byte[16]` supported by .NET itself. So let's take advantage of that and write our own type (shown in BARE schema):

```
type UUID data<16>
```

Because I want my message as small as possible I went with the `byte[16]` type, which equals to the BARE type `data<16>`. My encoder would take the `Guid`, converts it to `byte[]` and encodes it with `Encode_data_fixed_length`. The decoder would do the opposite.

```csharp
using System;
using BareNET;
byte[] Guid_Encoded(Guid guid)
{
    var guid_as_bytes = guid.ToByteArray();
    return Bare.Encode_data_fixed_length(16, guid_as_bytes);
}

Guid Guid_Decoded(byte[] data)
{
    var (guid_as_bytes, _) = Bare.Decode_data_fixed_length(16, data);
    return new Guid(guid_as_bytes);
}
```

Or we make use of the code generator and write a mapper to convert the `Guid`.

```csharp
using System;
using Bare.Msg; // Namespace of the generated code
byte[] Guid_Encoded(Guid guid)
{
    var uuid = new UUID(guid.ToByteArray());
    return Encoding.UUID_Encoded(uuid);
}

Guid Guid_Decoded(byte[] data)
{
    var uuid = Encoding.UUID_Encoded(data);
    return new Guid(uuid.Value);
}
```

## Limitations
- Reflection is not supported yet, so you have to write your own encoder / decoder or use the code generator
- Length of data and list types is limited to `Int32.MaxValue` (2.147.483.647)
- Using `optional<optional<>>` is not allowed
- Optional types need to differ between [reference type](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/reference-types) and [value type](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/value-types)
    - For value types use the methods `Encode_optional<T>` and `Decode_optional<T>`
    - For reference types use the methods `Encode_optional_ref<T>` and `Decode_optional_ref<T>`
