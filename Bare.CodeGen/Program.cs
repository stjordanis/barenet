﻿#nullable enable
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BareNET.Schema;

namespace Bare.CodeGen
{
    public class Program
    {
        private const string _default_namespace = "Bare.Msg";

        public static void Main(string[] args)
        {
            switch (CommandLine.Parse_command(args))
            {
                case Generate_from_File command:
                {
                    if (!File.Exists(command.Path_to_schema))
                    {
                        CommandLine.Print_error($"Given path of schema file leads to nowhere. Please check the path {command.Path_to_schema}");
                        return;
                    }

                    try
                    {
                        var schema = Read_schema(command.Path_to_schema);
                        var code = Generate_code(schema, command.Configuration);
                        Save_code_to_file(command.Output_path, code);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Print_error(ex.Message);
                    }

                    break;
                }

                case Generate_from_StdIn_to_StdOut command:
                {
                    try
                    {
                        var schema = Read_schema_from_stdin();
                        var code = Generate_code(schema, command.Configuration);
                        Save_code_to_stdout(code);
                    }
                    catch(Exception ex)
                    {
                        CommandLine.Write_error(ex);
                    }
                    break;
                }

                case Print_help:
                {
                    CommandLine.Print_help();
                    break;
                }
            }
        }

        private static List<UserType> Read_schema_from_stdin()
        {
            using var stream = Open_stdin();
            using var scanner = new Scanner(new StreamReader(stream));
            return Parse_schema(scanner);
        }

        private static Stream Open_stdin()
        {
            try
            {
                Console.InputEncoding = System.Text.Encoding.UTF8;
                return Console.OpenStandardInput();
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to parse schema from stdin.{Environment.NewLine}{ex.Message}");
            }
        }

        private static List<UserType> Read_schema(string path)
        {
            using var file = Open_file(path);
            using var scanner = new Scanner(new StreamReader(file));
            return Parse_schema(scanner);
        }

        private static FileStream Open_file(string path)
        {
            try
            {
                return File.OpenRead(path);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to read schema file '{path}'.{Environment.NewLine}{ex.Message}");
            }
        }

        private static List<UserType> Parse_schema(Scanner scanner)
        {
            try
            {
                return Parser.Parse(scanner).ToList();
            }
            catch (FormatException ex)
            {
                throw new Exception($"Syntax error in schema: {ex.Message}");
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed parse schema.{Environment.NewLine}{ex.Message}");
            }
        }

        private static string Generate_code(List<UserType> schema, Code_configuration configuration)
        {
            var @namespace = configuration.Namespace ?? _default_namespace;
            try
            {
                return configuration.TargetLanguage.HasValue && configuration.TargetLanguage.Value == TargetLanguage.FSharp
                    ? FSharp_code_generator.Generate(schema, @namespace, DateTime.Now, configuration.List_type, configuration.Encoding_classname, configuration.Identation_whitespaces)
                    : CSharp_code_generator.Generate(schema, @namespace, DateTime.Now, configuration.List_type, configuration.Encoding_classname, configuration.Identation_whitespaces);
            }
            catch (FormatException ex)
            {
                throw new Exception($"Schema error: {ex.Message}");
            }
            catch (Exception ex)
            {
                throw new Exception($"Internal error during code generation.{Environment.NewLine}{ex.Message}");
            }
        }

        private static void Save_code_to_file(string file, string code)
        {
            File.WriteAllText(file, code);
        }

        private static void Save_code_to_stdout(string code)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Out.Write(code);
        }
    }
}
