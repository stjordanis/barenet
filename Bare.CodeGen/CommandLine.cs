#nullable enable
using System;
using System.Linq;

namespace Bare.CodeGen
{
    internal enum TargetLanguage { CSharp, FSharp }

    internal readonly struct Code_configuration
    {
        public readonly TargetLanguage? TargetLanguage;
        public readonly string? Namespace;
        public readonly ListImplementation? List_type;
        public readonly string? Encoding_classname;
        public readonly int? Identation_whitespaces;

        public Code_configuration(TargetLanguage? targetLanguage, string? @namespace, ListImplementation? list_type, string? encoding_classname, int? identation_whitespaces)
        {
            TargetLanguage = targetLanguage;
            Namespace = @namespace;
            List_type = list_type;
            Encoding_classname = encoding_classname;
            Identation_whitespaces = identation_whitespaces;
        }
    }

    internal interface Command {}

    internal readonly struct Generate_from_File : Command
    {
        public readonly string Path_to_schema;
        public readonly string Output_path;
        public readonly Code_configuration Configuration;

        public Generate_from_File(string path_to_schema, string output_path, Code_configuration configuration)
        {
            Path_to_schema = path_to_schema;
            Output_path = output_path;
            Configuration = configuration;
        }
    }

    internal readonly struct Generate_from_StdIn_to_StdOut : Command
    {
        public readonly Code_configuration Configuration;

        public Generate_from_StdIn_to_StdOut(Code_configuration configuration)
        {
            Configuration = configuration;
        }
    }

    internal readonly struct Print_help : Command {}

    internal readonly struct Wrong_Arguments : Command {}


    internal enum Argument_Collector
    {
        None,
        Language,
        List_Type,
        Namespace,
        Classname,
        Identation_whitespaces
    }

    internal struct Argument_Holder
    {
        public string? Path_to_schema;
        public string? Output_path;
        public bool StdIn;
        public TargetLanguage? Language;
        public ListImplementation? List_type;
        public string? Namespace;
        public string? Encoding_classname;
        public int? Identation_whitespaces;
        public bool Help;
    }

    internal static class CommandLine
    {
        public static Command Parse_command(string[] args)
        {
            if (args.Length == 0)
            {
                Print_error("Missing arguments");
                return new Print_help();
            }

            try
            {
                var arguments =
                    args.Aggregate(
                        (holder: new Argument_Holder(), collector: Argument_Collector.None),
                        (state, arg) => Collect_argument(state.holder, state.collector, arg),
                        state => state.holder);

                if (arguments.Help) return new Print_help();

                if (arguments.StdIn)
                {
                    return new Generate_from_StdIn_to_StdOut(
                        new Code_configuration(
                            arguments.Language,
                            arguments.Namespace,
                            arguments.List_type,
                            arguments.Encoding_classname,
                            arguments.Identation_whitespaces));
                }

                if (arguments.Path_to_schema != null && arguments.Output_path != null)
                {
                    return new Generate_from_File(
                        arguments.Path_to_schema,
                        arguments.Output_path,
                        new Code_configuration(
                            arguments.Language,
                            arguments.Namespace,
                            arguments.List_type,
                            arguments.Encoding_classname,
                            arguments.Identation_whitespaces));
                }

                Print_error("Missing schema and output file arguments");
                return new Wrong_Arguments();
            }
            catch
            {
                return new Wrong_Arguments();
            }
        }

        private static (Argument_Holder, Argument_Collector) Collect_argument(Argument_Holder holder, Argument_Collector collector, string arg)
        {
            switch(collector)
            {
                case Argument_Collector.Language:
                {
                    if (arg == "cs") holder.Language = TargetLanguage.CSharp;
                    else if (arg == "fs") holder.Language = TargetLanguage.FSharp;
                    else
                    {
                        Print_error($"Invalid option for --lang: '{arg}'");
                        throw new Exception();
                    }

                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.List_Type:
                {
                    if (arg == "list") holder.List_type = ListImplementation.List;
                    else if (arg == "array") holder.List_type = ListImplementation.Array;
                    else if (arg == "readonly") holder.List_type = ListImplementation.ReadonlyCollection;
                    else
                    {
                        Print_error($"Invalid option for --list-type: '{arg}'");
                        Print_error($"Possible values \"list\", \"array\" or \"readonly\"");
                        throw new Exception();
                    }

                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.Namespace:
                {
                    holder.Namespace = arg;
                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.Classname:
                {
                    holder.Encoding_classname = arg;
                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.Identation_whitespaces:
                {
                    if (int.TryParse(arg, out var count))
                    {
                        holder.Identation_whitespaces = count;
                        return (holder, Argument_Collector.None);
                    }
                    Print_error($"Option for --whitespaces is not a number: '{arg}'");
                    throw new Exception();
                }

                default:
                {
                    if (arg == "--stdin")
                    {
                        holder.StdIn = true;
                        return (holder, Argument_Collector.None);
                    }
                    if (arg == "--help")
                    {
                        holder.Help = true;
                        return (holder, Argument_Collector.None);
                    }
                    if (arg == "-l" || arg == "--lang") return (holder, Argument_Collector.Language);
                    if (arg == "-lt" || arg == "--list-type") return (holder, Argument_Collector.List_Type);
                    if (arg == "-ns" || arg == "--namespace") return (holder, Argument_Collector.Namespace);
                    if (arg == "-class" || arg == "--classname") return (holder, Argument_Collector.Classname);
                    if (arg == "-ws" || arg == "--whitespaces") return (holder, Argument_Collector.Identation_whitespaces);

                    if (holder.Path_to_schema == null)
                    {
                        holder.Path_to_schema = arg;
                        return (holder, Argument_Collector.None);
                    }
                    else if (holder.Output_path == null)
                    {
                        holder.Output_path = arg;
                        return (holder, Argument_Collector.None);
                    }

                    Print_error($"Unknown argument '{arg}'");
                    throw new Exception();
                }
            }
        }

        public static void Print_help()
        {
            Print_in_color(ConsoleColor.DarkGray, "Usage");
            Print_in_color(ConsoleColor.White, "Read schema from file and writes to out file".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "bare <schema-file> <out-file>".With_indentation(2, null));
            Print_in_color(ConsoleColor.White, "Read schema from stdin and writes to stdout".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "bare --stdin".With_indentation(2, null));

            Console.WriteLine();
            Print_in_color(ConsoleColor.DarkGray, "Examples");
            Print_in_color(ConsoleColor.Blue, "bare schema.bare Messages.cs".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "echo \"type Token data<16>\" | bare --stdin | less".With_indentation(1, null));

            Console.WriteLine();
            Print_in_color(ConsoleColor.DarkGray, "Options");
            Print_in_color(ConsoleColor.White, "--help | Prints this help text".With_indentation(1, null));

            Print_in_color(ConsoleColor.White, "--stdin | Reads schema from stdin and writes code to stdout".With_indentation(1, null));

            Print_in_color(ConsoleColor.White, "-l --lang <fs|cs>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "The target language to produce code for".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: cs".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-lt --list-type <list|array|readonly>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Defines which underlying type the code generator should use for lists".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: list".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-ns --namespace <name>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Types and encoding class will be in the given namespace".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: \"Bare.Msg\"".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-class --classname <name>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Name of the encoding class".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: \"Encoding\"".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-ws --whitespaces <count>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Count of whitespaces used for indentation. When set the C# code will be indentated with whitespaces instead of tabs.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default for F#: 2".With_indentation(2, null));
        }

        public static void Print_info(string text) => Console.WriteLine(text);
        public static void Print_error(string text) => Print_in_color(ConsoleColor.Red, text);

        private static void Print_in_color(ConsoleColor color, string text)
        {
            var tmp = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = tmp;
        }

        internal static void Write_error(Exception ex)
        {
            Console.Error.WriteLine(ex.Message);
            Console.Error.WriteLine(ex.StackTrace);
        }
    }
}