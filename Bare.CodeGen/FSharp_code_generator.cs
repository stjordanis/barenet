#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BareNET.Schema;

namespace Bare.CodeGen
{
    internal interface FSharp_Type {}

    internal readonly struct FSharp_Enum : FSharp_Type
    {
        public readonly string Name;
        public readonly FSharp_Enum_Value[] Values;

        public FSharp_Enum(string name, FSharp_Enum_Value[] values)
        {
            Name = name;
            Values = values;
        }
    }

    internal readonly struct FSharp_Enum_Value
    {
        public readonly string Name;
        public readonly uint Value;

        public FSharp_Enum_Value(string name, uint value)
        {
            Name = name;
            Value = value;
        }
    }

    internal readonly struct FSharp_Alias : FSharp_Type
    {
        public readonly string Name;
        public readonly string FSharpType;
        public readonly string Encoder;
        public readonly bool Encoder_can_fail;
        public readonly string Decoder;

        public FSharp_Alias(string name, string fSharpType, string encoder, bool encoder_can_fail, string decoder)
        {
            Name = name;
            FSharpType = fSharpType;
            Encoder = encoder;
            Encoder_can_fail = encoder_can_fail;
            Decoder = decoder;
        }
    }

    internal readonly struct FSharp_Union : FSharp_Type
    {
        public readonly string Name;
        public readonly FSharp_Union_case[] Cases;

        public FSharp_Union(string name, FSharp_Union_case[] cases)
        {
            Name = name;
            Cases = cases;
        }
    }

    internal readonly struct FSharp_Union_case
    {
        public readonly string Name;
        public readonly int Identifier;
        public readonly string? Payload_FSharpType;
        public readonly string Encoder;
        public readonly bool Encoder_can_fail;
        public readonly string Decoder;

        public FSharp_Union_case(string name, int identifier, string? payload_FSharpType, string encoder, bool encoder_can_fail, string decoder)
        {
            Name = name;
            Identifier = identifier;
            Payload_FSharpType = payload_FSharpType;
            Encoder = encoder;
            Encoder_can_fail = encoder_can_fail;
            Decoder = decoder;
        }
    }

    internal readonly struct FSharp_Record : FSharp_Type
    {
        public readonly string Name;
        public readonly FSharp_Record_field[] Fields;

        public FSharp_Record(string name, FSharp_Record_field[] fields)
        {
            Name = name;
            Fields = fields;
        }
    }

    internal readonly struct FSharp_Record_field
    {
        public readonly string Name;
        public readonly string FSharpType;
        public readonly string Encoder;
        public readonly string Decoder;

        public FSharp_Record_field(string name, string fSharpType, string encoder, string decoder)
        {
            Name = name;
            FSharpType = fSharpType;
            Encoder = encoder;
            Decoder = decoder;
        }
    }

    internal static class FSharp_code_generator
    {
        private static Func<string, string> ListType = type => $"{type} array";
        private static string ListCollector = "Array.ofSeq";
        private static string Generated_encoding_class = Utils.Default_generated_encoding_class;
        private static int Indentation_whitespaces;

        public static string Generate(List<UserType> schema, string @namespace, DateTime generationTime, ListImplementation? listImplementation, string? encoding_classname, int? indentation_whitespaces)
        {
            if (listImplementation.HasValue)
            {
                switch(listImplementation.Value)
                {
                    case ListImplementation.List:
                    case ListImplementation.ReadonlyCollection:
                    {
                        ListType = type => $"{type} list";
                        ListCollector = "List.ofSeq";
                        break;
                    }
                }
            }
            if (encoding_classname != null) Generated_encoding_class = encoding_classname;
            Indentation_whitespaces = indentation_whitespaces ?? 2;

            var builder = new StringBuilder();
            var types = Types_of(schema).ToList();

            Utils.Generate_header(generationTime, builder);
            builder.AppendLine($"namespace {@namespace}");
            builder.AppendLine();
            builder.AppendJoin($"{Environment.NewLine}{Environment.NewLine}", Generate_types(types));
            builder.AppendLine();
            builder.AppendLine();
            builder.AppendLine(Generate_encoding_module(types));
            return builder.ToString();
        }

        private static IEnumerable<(UserType Type, int Priority)> Type_with_priorities(List<UserType> schema)
        {
            var type_dependencies =
                schema.Aggregate(new Dictionary<string, List<string>>(), (dependencies, type) => {
                    switch (type)
                    {
                        case EnumUserType enumType:
                            if (dependencies.ContainsKey(enumType.Name)) throw new FormatException($"Type '{enumType.Name}' is declared multiple times");
                            dependencies.Add(enumType.Name, new List<string>());
                            break;

                        case NamedUserType userType:
                        {
                            if (dependencies.ContainsKey(userType.Name)) throw new FormatException($"Type '{userType.Name}' is declared multiple times");
                            var userTypeUsages = new List<string>();
                            Find_dependencies(userTypeUsages, userType.Type);
                            if (!dependencies.ContainsKey(userType.Name)) dependencies.Add(userType.Name, userTypeUsages);
                            break;
                        }
                    }
                    return dependencies;
                });

            return schema
                .Select(type => type switch
                {
                    EnumUserType => (type, 0),
                    NamedUserType userType => (type, Determine_priority(userType.Name, userType.Name, type_dependencies)),
                    _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
                });
        }

        private static int Determine_priority(string typename, string parents, Dictionary<string, List<string>> dependencies)
        {
            if (!dependencies.ContainsKey(typename)) throw new FormatException($"Unknown type '{typename}' in '{parents}'!" );
            return dependencies[typename]
                .Select(name => Determine_priority(name, $"{name} <- {parents}", dependencies))
                .Sum() + 1;
        }

        private static void Find_dependencies(List<string> dependencies, NonEnumType type)
        {
            switch(type)
            {
                case OptionalType optionalType:
                    Find_dependencies(dependencies, optionalType.Type);
                    break;

                case ListType listType:
                    Find_dependencies(dependencies, listType.Type);
                    break;

                case MapType mapType:
                    Find_dependencies(dependencies, mapType.ValueType);
                    break;

                case StructType structType:
                    foreach (var field in structType.Fields) Find_dependencies(dependencies, field.Type);
                    break;

                case UnionType unionType:
                    foreach (var member in unionType.Members) Find_dependencies(dependencies, member.Type);
                    break;

                case UserTypeName userType:
                {
                    dependencies.Add(userType.Name);
                    break;
                }
            }
        }

        private static IEnumerable<FSharp_Type> Types_of(List<UserType> schema)
        {
            return Type_with_priorities(schema)
                .OrderBy(_ => _.Priority)                                                                                                                  
                .Select(_ => _.Type)
                .Aggregate(
                    (new Dictionary<string, FSharp_Type>(), new Dictionary<string, bool>(), new HashSet<string>()),
                    (state, type) =>
                    {
                        var (types, canFailLookup, voidTypes) = state;
                        switch(type)
                        {
                            case EnumUserType enumType:
                            {
                                canFailLookup.Add(enumType.Name, true);
                                var values =
                                    enumType.Type.Values
                                        .Aggregate(
                                            (value: 0u, values: new List<FSharp_Enum_Value>()),
                                            (tuple, entry) =>
                                            {
                                                var value = entry.Value ?? tuple.value;
                                                tuple.values.Add(new FSharp_Enum_Value(entry.Name, value));
                                                return (value + 1, tuple.values);
                                            },
                                            tuple => tuple.values.ToArray());
                                types.Add(enumType.Name, new FSharp_Enum(enumType.Name, values));
                                break;
                            }

                            case NamedUserType { Name: var name, Type: UnionType unionType }:
                                canFailLookup.Add(name, true);
                                Register_union(types, name, unionType.Members, canFailLookup, voidTypes);
                                break;

                            case NamedUserType { Name: var name, Type: StructType structType }:
                                canFailLookup.Add(name, true);
                                Register_record(types, name, structType.Fields, canFailLookup, voidTypes);
                                break;

                            case NamedUserType { Name: var name, Type: var userType }:
                            {
                                if (userType is PrimitiveType { Kind: TypeKind.Void }) voidTypes.Add(name);
                                var can_fail = Can_fail(userType, canFailLookup);
                                canFailLookup.Add(name, can_fail);
                                var (fsharpType, encoder, decoder) = Type_Encoder_Decoder(types, name, userType, canFailLookup, voidTypes);
                                types.Add(name, new FSharp_Alias(name, fsharpType, encoder, can_fail, decoder));
                                break;
                            }
                        }
                        return (types, canFailLookup, voidTypes);
                    },
                    state => state.Item1)
                .Values;
        }

        private static void Register_union(Dictionary<string, FSharp_Type> types, string name, IEnumerable<UnionMember> union_members, Dictionary<string, bool> canFailLookup, HashSet<string> voidTypes)
        {
            types.Add(
                name,
                new FSharp_Union(
                    name,
                    union_members
                        .Aggregate(
                            (0, new List<(int, UnionMember)>()),
                            (state, member) =>
                            {
                                var (identifier, members) = state;
                                if (member.Identifier.HasValue)
                                {
                                    members.Add((member.Identifier.Value, member));
                                    return (member.Identifier.Value, members);
                                }
                                members.Add((identifier++, member));
                                return (identifier, members);
                            },
                            state => state.Item2)
                        .Select(identified_member =>
                        {
                            var (identifier, member) = identified_member;
                            string caseName = member.Type switch
                            {
                                PrimitiveType primitiveType => $"{name}_{primitiveType.Kind}",
                                BareNET.Schema.ListType => $"{name}_List",
                                MapType => $"{name}_Map",
                                OptionalType => $"{name}_Optional",
                                StructType => $"{name}_Struct",
                                UnionType => $"{name}_Union",
                                UserTypeName userType => $"{name}_{userType.Name}",
                                _ => throw new ArgumentOutOfRangeException(nameof(member.Type), member.Type, null)
                            };

                            var (fsharpType, encoder, decoder) = Type_Encoder_Decoder(types, name, member.Type, canFailLookup, voidTypes);
                            var payload =
                                member.Type is UserTypeName t && voidTypes.Contains(t.Name)
                                    ? null
                                    : fsharpType;

                            return new FSharp_Union_case(caseName, identifier, payload, encoder, Can_fail(member.Type, canFailLookup), decoder);
                        })
                        .ToArray()));
        }

        private static void Register_record(Dictionary<string, FSharp_Type> types, string name, StructField[] fields, Dictionary<string, bool> canFailLookup, HashSet<string> voidTypes)
        {
            types.Add(
                name,
                new FSharp_Record(
                    name,
                    fields
                        .Select(field =>
                        {
                            var (fsharpType, encoder, decoder) = Type_Encoder_Decoder(types, name, field.Type, canFailLookup, voidTypes);
                            return new FSharp_Record_field(field.Name, fsharpType, As_Encoder(encoder, field.Type, canFailLookup), decoder);
                        })
                        .ToArray()));
        }

        private static IEnumerable<string> Generate_types(IEnumerable<FSharp_Type> types)
        {
            foreach(var fsharpType in types)
            {
                switch(fsharpType)
                {
                    case FSharp_Enum type:
                        yield return Generate_enum(type);
                        break;

                    case FSharp_Union type:
                        yield return Generate_union(type);
                        break;

                    case FSharp_Record type:
                        yield return Generate_record(type);
                        break;

                    case FSharp_Alias type:
                        yield return $"type {type.Name} = {type.FSharpType}";
                        break;
                }
            }
        }

        private static string Generate_enum(FSharp_Enum type)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"type {type.Name} =");
            builder.AppendJoin(Environment.NewLine, type.Values.Select(value => $"| {value.Name} = {value.Value}".With_indentation(1, Indentation_whitespaces)));
            return builder.ToString();
        }

        private static string Generate_union(FSharp_Union union)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"type {union.Name} =");
            builder.AppendJoin(
                Environment.NewLine,
                union.Cases.Select(@case =>
                {
                    var payload =
                        @case.Payload_FSharpType != null
                            ? $" of {@case.Payload_FSharpType}"
                            : "";
                    return $"| {@case.Name}{payload}".With_indentation(1, Indentation_whitespaces);
                }));
            return builder.ToString();
        }

        private static string Generate_record(FSharp_Record record)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"type {record.Name} =");
            builder.AppendLine("{".With_indentation(1, Indentation_whitespaces));
            foreach(var field in record.Fields) builder.AppendLine($"{field.Name}: {field.FSharpType}".With_indentation(2, Indentation_whitespaces));
            builder.Append("}".With_indentation(1, Indentation_whitespaces));
            return builder.ToString();
        }

        private static string TypeKind_as_FSharp_type(TypeKind kind)
        {
            return kind switch
            {
                TypeKind.UInt => "uint64",
                TypeKind.U8 => "uint8",
                TypeKind.U16 => "uint16",
                TypeKind.U32 => "uint",
                TypeKind.U64 => "uint64",
                TypeKind.Int => "int64",
                TypeKind.I8 => "int8",
                TypeKind.I16 => "int16",
                TypeKind.I32 => "int",
                TypeKind.I64 => "int64",
                TypeKind.F32 => "float",
                TypeKind.F64 => "double",
                TypeKind.Bool => "bool",
                TypeKind.String => "string",
                TypeKind.Data => "byte array",
                TypeKind.Void => "unit",
                _ => throw new ArgumentOutOfRangeException(nameof(kind), kind, null)
            };
        }

        private static string Anonymous_record_FSharp_type(IEnumerable<FSharp_Record_field> fields)
        {
            var builder = new StringBuilder();
            builder.Append("{| ");
            builder.AppendJoin(" ; ", fields.Select(field => $"{field.Name}: {field.FSharpType}"));
            builder.Append(" |}");
            return builder.ToString();
        }

        private static string Generate_encoding_module(IEnumerable<FSharp_Type> types)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"module {Generated_encoding_class} =");
            builder.AppendJoin($"{Environment.NewLine}{Environment.NewLine}", types.SelectMany(Generate_encoding_functions).Select(_ => _.With_indentation(1, Indentation_whitespaces)));
            return builder.ToString();
        }

        private static IEnumerable<string> Generate_encoding_functions(FSharp_Type type)
        {
            switch(type)
            {
                case FSharp_Enum enumType:
                    return Generate_functions(enumType.Name, true, $"{Utils.BareClass}.encode_enum<{enumType.Name}>", $"{Utils.BareClass}.decode_enum<{enumType.Name}>");

                case FSharp_Record recordType:
                    return Generate_record_functions(recordType.Name, recordType.Fields.ToList());

                case FSharp_Union unionType:
                    return Generate_union_functions(unionType);

                case FSharp_Alias aliasType:
                    return Generate_functions(aliasType.Name, aliasType.Encoder_can_fail, aliasType.Encoder, aliasType.Decoder);

                default:
                    return new string[0];
            }
        }

        private static (string fsharpType, string encoder, string decoder) Type_Encoder_Decoder(Dictionary<string, FSharp_Type> types, string basename, NonEnumType type, Dictionary<string, bool> canFailLookup, HashSet<string> voidTypes)
        {
            switch(type)
            {
                case PrimitiveType primitiveType:
                    return (TypeKind_as_FSharp_type(primitiveType.Kind), PrimitiveEncoder(primitiveType), PrimitiveDecoder(primitiveType));

                case ListType listType:
                {
                    var (fsharpType, encoder, decoder) = Type_Encoder_Decoder(types, basename, listType.Type, canFailLookup, voidTypes);
                    var prefix =
                        listType.Length.HasValue
                            ? $"_fixed_length {listType.Length.Value}"
                            : "";
                    return (
                        ListType(fsharpType),
                        $"{Utils.BareClass}.encode_list{prefix} ({As_Encoder(encoder, listType.Type, canFailLookup)})",
                        $"{Utils.BareClass}.decode_complex {ListCollector} |> {Utils.BareClass}.apply ({Utils.BareClass}.decode_list{prefix} ({decoder}))");
                }

                case MapType mapType:
                {
                    if (mapType.KeyType is PrimitiveType keyType && keyType.Kind != TypeKind.Void)
                    {
                        var (fsharpType, valueEncoder, valueDecoder) = Type_Encoder_Decoder(types, basename, mapType.ValueType, canFailLookup, voidTypes);
                        var keyEncoder = As_Encoder(PrimitiveEncoder(keyType), keyType, canFailLookup);
                        var keyDecoder = PrimitiveDecoder(keyType);
                        return (
                            $"Map<{TypeKind_as_FSharp_type(keyType.Kind)}, {fsharpType}>",
                            $"{Utils.BareClass}.encode_map ({keyEncoder}) ({As_Encoder(valueEncoder, mapType.ValueType, canFailLookup)})",
                            $"{Utils.BareClass}.decode_map ({keyDecoder}) ({valueDecoder})"
                        );
                    }

                    throw new FormatException($"Type {mapType.KeyType} can't be used as key type for a map. Only primitive types which is not void can be used");
                }

                case OptionalType optionalType:
                {
                    var (fsharpType, encoder, decoder) = Type_Encoder_Decoder(types, basename, optionalType.Type, canFailLookup, voidTypes);
                    return (
                        $"{fsharpType} option",
                        $"{Utils.BareClass}.encode_optional ({As_Encoder(encoder, optionalType.Type, canFailLookup)})",
                        $"{Utils.BareClass}.decode_optional ({decoder})"
                    );
                }

                case StructType structType:
                {
                    var fields =
                        structType.Fields
                            .Select(field =>
                            {
                                var (field_fsharpType, encoder, decoder) = Type_Encoder_Decoder(types, $"{basename}_{field.Name}", field.Type, canFailLookup, voidTypes);
                                return new FSharp_Record_field(field.Name, field_fsharpType, As_Encoder(encoder, field.Type, canFailLookup), decoder);
                            })
                            .ToList();
                    var fsharpType = Anonymous_record_FSharp_type(fields);
                    return (
                        fsharpType,
                        Anonymous_record_encoder(fsharpType, fields),
                        Anonymous_record_decoder(fields)
                    );
                }

                case UnionType unionType:
                {
                    var name = $"{basename}_Union";
                    Register_union(types, name, unionType.Members, canFailLookup, voidTypes);
                    return (name, $"of{name}", $"decode_{name}");
                }

                case UserTypeName userType:
                    return (userType.Name, $"of{userType.Name}", $"decode_{userType.Name}");

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private static string As_Encoder(string encoder, NonEnumType type, Dictionary<string, bool> userTypesCanFail)
        {
            return Can_fail(type, userTypesCanFail)
                ? encoder
                : $"{Utils.BareClass}.success ({encoder})";
        }

        private static bool Can_fail(NonEnumType type, Dictionary<string, bool> userTypesCanFail)
        {
            return type switch
            {
                BareNET.Schema.ListType => true,
                MapType => true,
                OptionalType => true,
                PrimitiveType primitiveType =>
                    primitiveType.Kind switch
                    {
                        TypeKind.Int => false,
                        TypeKind.I8 => false,
                        TypeKind.I16 => false,
                        TypeKind.I32 => false,
                        TypeKind.I64 => false,
                        TypeKind.UInt => false,
                        TypeKind.U8 => false,
                        TypeKind.U16 => false,
                        TypeKind.U32 => false,
                        TypeKind.U64 => false,
                        TypeKind.F32 => true,
                        TypeKind.F64 => true,
                        TypeKind.Bool => false,
                        TypeKind.String => false,
                        TypeKind.Data => primitiveType.Length.HasValue,
                        TypeKind.Void => false,
                        _ => throw new ArgumentOutOfRangeException()
                    },
                StructType => true,
                UnionType => true,
                UserTypeName userType => userTypesCanFail.ContainsKey(userType.Name) ? userTypesCanFail[userType.Name] : throw new FormatException($"Unknown type '{userType.Name}'"),
                _ => throw new ArgumentOutOfRangeException(nameof(type))
            };
        }

        private static string PrimitiveEncoder(PrimitiveType primitiveType)
        {
            return primitiveType.Kind switch
            {
                TypeKind.Int => $"{Utils.BareClass}.encode_int",
                TypeKind.I8 => $"{Utils.BareClass}.encode_i8",
                TypeKind.I16 => $"{Utils.BareClass}.encode_i16",
                TypeKind.I32 => $"{Utils.BareClass}.encode_i32",
                TypeKind.I64 => $"{Utils.BareClass}.encode_i64",
                TypeKind.UInt => $"{Utils.BareClass}.encode_uint",
                TypeKind.U8 => $"{Utils.BareClass}.encode_u8",
                TypeKind.U16 => $"{Utils.BareClass}.encode_u16",
                TypeKind.U32 => $"{Utils.BareClass}.encode_u32",
                TypeKind.U64 => $"{Utils.BareClass}.encode_u64",
                TypeKind.F32 => $"{Utils.BareClass}.encode_f32",
                TypeKind.F64 => $"{Utils.BareClass}.encode_f64",
                TypeKind.Bool => $"{Utils.BareClass}.encode_bool",
                TypeKind.String => $"{Utils.BareClass}.encode_string",
                TypeKind.Data =>
                    primitiveType.Length.HasValue
                        ? $"{Utils.BareClass}.encode_data_fixed_length {primitiveType.Length.Value}"
                        : $"{Utils.BareClass}.encode_data",
                TypeKind.Void => "fun _ -> [||]",
                _ => throw new ArgumentOutOfRangeException(),
            };
        }

        private static string PrimitiveDecoder(PrimitiveType primitiveType)
        {
            return primitiveType.Kind switch
            {
                TypeKind.Int => $"{Utils.BareClass}.decode_int",
                TypeKind.I8 => $"{Utils.BareClass}.decode_i8",
                TypeKind.I16 => $"{Utils.BareClass}.decode_i16",
                TypeKind.I32 => $"{Utils.BareClass}.decode_i32",
                TypeKind.I64 => $"{Utils.BareClass}.decode_i64",
                TypeKind.UInt => $"{Utils.BareClass}.decode_uint",
                TypeKind.U8 => $"{Utils.BareClass}.decode_u8",
                TypeKind.U16 => $"{Utils.BareClass}.decode_u16",
                TypeKind.U32 => $"{Utils.BareClass}.decode_u32",
                TypeKind.U64 => $"{Utils.BareClass}.decode_u64",
                TypeKind.F32 => $"{Utils.BareClass}.decode_f32",
                TypeKind.F64 => $"{Utils.BareClass}.decode_f64",
                TypeKind.Bool => $"{Utils.BareClass}.decode_bool",
                TypeKind.String => $"{Utils.BareClass}.decode_string",
                TypeKind.Data =>
                    primitiveType.Length.HasValue
                        ? $"{Utils.BareClass}.decode_data_fixed_length {primitiveType.Length.Value}"
                        : $"{Utils.BareClass}.decode_data",
                TypeKind.Void => $"{Utils.BareClass}.from_value ()",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        private static string Anonymous_record_encoder(string fsharpType, List<FSharp_Record_field> fields)
        {
            var builder = new StringBuilder();
            var first = fields.First();
            builder.AppendFormat("fun (value: {0}) -> ({1}) value.{2}", fsharpType, first.Encoder, first.Name);
            foreach (var field in fields.Skip(1))
            {
                builder.AppendFormat(" |> {0}.andThen ({1}) value.{2}", Utils.BareClass, field.Encoder, field.Name);
            }

            return builder.ToString();
        }

        private static string Anonymous_record_decoder(List<FSharp_Record_field> fields)
        {
            var builder = new StringBuilder();
            builder.AppendFormat("{0}.decode_complex (fun ", Utils.BareClass);
            builder.AppendJoin(" ", fields.Select(f => $"({f.Name}: {f.FSharpType})"));
            builder.Append(" -> {| ");
            builder.AppendJoin(" ; ", fields.Select(f => $"{f.Name} = {f.Name}"));
            builder.Append(" |})");
            foreach (var field in fields) builder.AppendFormat(" |> {0}.apply ({1})", Utils.BareClass, field.Decoder);

            return builder.ToString();
        }

        private static IEnumerable<string> Generate_functions(string typeName, bool canFail, string encoder, string decoder)
        {
            var builder = new StringBuilder();
            builder.AppendFormat("let of{0} (value: {0}) : {1} =", typeName, canFail ? "BareNET.Encoding_Result" : "byte array");
            builder.AppendLine();
            builder.AppendFormat("value |> {0}".With_indentation(1, Indentation_whitespaces), encoder);
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let decode_{0} : BareNET.Decoder<{0}> =", typeName);
            builder.AppendLine();
            builder.Append(decoder.With_indentation(1, Indentation_whitespaces));
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let to{0} (data: byte array) : Result<{0}, string> =", typeName);
            builder.AppendLine();
            builder.AppendFormat("decode_{0} data |> Result.map fst".With_indentation(1, Indentation_whitespaces), typeName);
            yield return builder.ToString();
        }

        private static IEnumerable<string> Generate_record_functions(string name, List<FSharp_Record_field> fields)
        {
            var builder = new StringBuilder();
            var first = fields.First();
            builder.AppendFormat("let of{0} (value: {0}) : BareNET.Encoding_Result =", name);
            builder.AppendLine();
            builder.AppendFormat("({0}) value.{1}".With_indentation(1, Indentation_whitespaces), first.Encoder, first.Name);
            builder.AppendLine();
            builder.AppendJoin(
                Environment.NewLine,
                fields
                    .Skip(1)
                    .Select(field => $"|> {Utils.BareClass}.andThen ({field.Encoder}) value.{field.Name}".With_indentation(1, Indentation_whitespaces)));
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let decode_{0} : BareNET.Decoder<{0}> =", name);
            builder.AppendLine();
            builder.AppendFormat("{0}.decode_complex (fun ".With_indentation(1, Indentation_whitespaces), Utils.BareClass);
            builder.AppendJoin(" ", fields.Select(f => $"({f.Name}: {f.FSharpType})"));
            builder.Append(" -> { ");
            builder.AppendJoin(" ; ", fields.Select(f => $"{f.Name} = {f.Name}"));
            builder.AppendLine(" })");
            builder.AppendJoin(
                Environment.NewLine,
                fields.Select(field => $"|> {Utils.BareClass}.apply ({field.Decoder})".With_indentation(1, Indentation_whitespaces)));
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let to{0} (data: byte array) : Result<{0}, string> =", name);
            builder.AppendLine();
            builder.AppendFormat("decode_{0} data |> Result.map fst".With_indentation(1, Indentation_whitespaces), name);
            yield return builder.ToString();
        }

        private static IEnumerable<string> Generate_union_functions(FSharp_Union union)
        {
            var builder = new StringBuilder();
            builder.AppendFormat("let private encoding_definition_{0} (value: {0}) : BareNET.Union_Case =", union.Name);
            builder.AppendLine();
            builder.AppendLine("match value with".With_indentation(1, Indentation_whitespaces));
            builder.AppendJoin(
                Environment.NewLine,
                union.Cases.Select(@case =>
                    @case.Payload_FSharpType != null
                        ? $"| {@case.Name} payload -> payload |> {@case.Encoder} |> {Utils.BareClass}.{(@case.Encoder_can_fail ? "failable_case" : "union_case")} {@case.Identifier}u".With_indentation(1, Indentation_whitespaces)
                        : $"| {@case.Name} -> {Utils.BareClass}.void_case {@case.Identifier}u".With_indentation(1, Indentation_whitespaces)));
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let private decoding_definition_{0} (identifier: uint) : BareNET.Decoder<{0}> =", union.Name);
            builder.AppendLine();
            builder.AppendLine("match identifier with".With_indentation(1, Indentation_whitespaces));
            foreach (var @case in union.Cases)
            {
                if (@case.Payload_FSharpType == null)
                {
                    builder.AppendFormat("| {0}u -> {1}.from_value {2}".With_indentation(1, Indentation_whitespaces), @case.Identifier, Utils.BareClass, @case.Name);
                }
                else
                {
                    builder.AppendFormat("| {0}u -> {1}.decode_complex {2} |> {1}.apply ({3})".With_indentation(1, Indentation_whitespaces), @case.Identifier, Utils.BareClass, @case.Name, @case.Decoder);
                }
                builder.AppendLine();
            }
            builder.AppendFormat("| identifier -> {0}.error (sprintf \"missing decoder for identifier %i\" identifier)".With_indentation(1, Indentation_whitespaces), Utils.BareClass);
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let of{0} (value: {0}) : BareNET.Encoding_Result =", union.Name);
            builder.AppendLine();
            builder.AppendFormat("{0}.encode_union encoding_definition_{1} value".With_indentation(1, Indentation_whitespaces), Utils.BareClass, union.Name);
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let decode_{0} : BareNET.Decoder<{0}> =", union.Name);
            builder.AppendLine();
            builder.AppendFormat("{0}.decode_union decoding_definition_{1}".With_indentation(1, Indentation_whitespaces), Utils.BareClass, union.Name);
            yield return builder.ToString();

            builder.Clear();
            builder.AppendFormat("let to{0} (data: byte array) : Result<{0}, string> =", union.Name);
            builder.AppendLine();
            builder.AppendFormat("decode_{0} data |> Result.map fst".With_indentation(1, Indentation_whitespaces), union.Name);
            yield return builder.ToString();
        }
    }
}