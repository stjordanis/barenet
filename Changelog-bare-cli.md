# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2021-03-04
### Changed
- [BREAKING] For F# code, encoding functions will be named as `to[Typename]` and value decoding function as `of[Typename]`.
	- This follows the naming conventions like in `List.ofSeq` and `List.toSeq`.
	- Generated decoder will still be named as `decode_[Typename]`.
	- Thanks to Alex Corrado!
- [CodeGen][F#] Don't expose union encode and decode definitions.
- CLI has readable, colored and meaningful error messages. No more error message hidden in a call stack!

### Fixed
- [F#] Type order will be determined correctly. The algorithm will consider nested usages.

## [0.2.0] - 2021-03-03
### Added
- [CodeGen] Generation of F# code. `bare schema.bare out.fs --lang fs`
- Option to read schema from stdin and write code to stdout. `bare --stdin`
- Option to change namespace of generated code. `bare --namespace My_own_Namespace`
- Option to change name of encoding class. `bare --classname Stuff_n_Things`
- Option to change the type for list values. `bare --list-type <list|array|readonly>`
  - F# will map `array` to `T array`
  - F# will map `list` and `readonly` to `T list`
  - C# will map `array` to `T[]`
  - C# will map `list` to `List<T>`
  - C# will map `readonly` to `ReadOnlyCollection<T>`
- Option to change the whitespace indentation. `bare --whitespace 4`
  - For C# this switches to whitespace indentation instead of tabs.
- CLI can print a beautiful help message. `bare --help`

### Fixed
- [CodeGen] Generator will not break struct fields CamelCase. See #3

## [0.1.2] - 2021-02-14
### Fixed
- [AST] Parser can now handle end of stream and won't fail if a schema ends with a trailing newline. See #1

## [0.1.1] - 2021-02-11
### Fixed
- [CodeGen] Structs with single field will now encode the field correctly.

## [0.1.0] - 2021-02-10
### Added
- Initial release